module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
    'postcss-pxtorem': {
      rootValue: 16,
      propList: ['font', 'width', 'height', 'min-width', 'min-height', 'padding', 'margin', 'font-size', 'line-height']
    }
  }
};
