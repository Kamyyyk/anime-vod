type Locale = 'pl' | 'en'

interface Children {
    children?: React.ReactNode
}

interface LayoutProps extends Children {
    params: {locale: Locale}
}
