import Image from 'next/image';
import YourNameBackground from '@/app/assets/images/backgrounds/your-name-background.png';
import PlaceHolderLogo from '@/app/assets/images/logos/placeholder-logo.svg';

export default function Home() {
  return (
    <div className="flex justify-center">
      <div className="relative max-w-[1920px] after:absolute after:left-0 after:top-0 after:h-full after:w-full after:bg-gradient-to-t after:from-black after:opacity-100">
        <div className="absolute left-1/2 top-1/2 z-10 -translate-x-1/2 -translate-y-1/2 transform">
          <div className="flex flex-row gap-24">
            <div className="text-white">
              <div className="flex items-center gap-6">
                <Image src={PlaceHolderLogo} alt="placeholder-logo" />
                <h2 className="text-5xl">Placeholder</h2>
              </div>
              <p className="mt-16 text-center">Video from above</p>
            </div>

            <div className="relative flex flex-col gap-12 text-white before:absolute before:left-[-50px] before:top-[-100px] before:h-[330px] before:w-[1px] before:bg-white">
              <h1 className="text-4xl">
                Stream anime from the <br /> cloud
              </h1>
              <p className="text-xl">Starting at $7.99/month</p>
            </div>
          </div>
        </div>
        <Image width={1920} src={YourNameBackground} alt="landing-page-background" />
      </div>
    </div>
  );
}
